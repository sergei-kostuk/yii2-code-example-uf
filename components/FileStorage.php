<?php

namespace common\components\Storage;

use Yii;
use yii\web\UploadedFile;
use yii\helpers\StringHelper;
use yii\helpers\FileHelper;

class FileStorage
{
    const ERROR_NO_FILE = 1;
    const ERROR_EMPTY_FILE = 2;
    const ERROR_WRONG_FORMAT = 3;
    const ERROR_UPLOAD_ERROR = 4;

    const DIR_USER = 'user';
    const DIR_MEDIA = 'media';
    const DIR_PRODUCT = 'product';
    const DIR_QUOTE = 'quote';
    const DIR_SLIDER = 'slider';

    private $directories = [
        'User' => self::DIR_USER,
        'Media' => self::DIR_MEDIA,
        'Product' => self::DIR_PRODUCT,
        'Quote' => self::DIR_QUOTE,
        'Slider' => self::DIR_SLIDER,
    ];

    public static $imageMimeTypes = ['image/jpeg', 'image/png', 'image/gif'];
    public static $videoMimeTypes = ['video/quicktime', 'video/mp4', 'video/webm', 'video/ogg', 'video/x-msvideo'];

    protected $_storagePath = null;
    protected $_storageUrl = null;

    protected $_errorCode = null;

    /**
     * Storage constructor.
     */
    public function __construct()
    {
        $this->_storagePath = Yii::getAlias('@file.path');
        $this->_storageUrl = Yii::getAlias('@file.url');
    }

    /**
     * @return array
     */
    protected static function getMessages()
    {
        return [
            self::ERROR_NO_FILE => Yii::t('app', 'No file uploaded.'),
            self::ERROR_EMPTY_FILE => Yii::t('app', 'The file is empty.'),
            self::ERROR_WRONG_FORMAT => Yii::t('app', 'Wrong file format.'), //  Allowed formats are jpg, png and gif.
            self::ERROR_UPLOAD_ERROR => Yii::t('app', 'File uploading error. No writing/modifying file permission.'),
        ];
    }

    /**
     * @param $entity
     * @param $fieldName
     * @param null $inputName
     * @return null|string
     * @throws \yii\base\InvalidConfigException
     */
    public function getFileName($entity, $fieldName, $inputName = null)
    {
        $file = $this->getFile($entity, $fieldName, $inputName);

        $fileName = null;
        if (!empty($file)) {
            $this->deleteFile($entity, $fieldName);
            if (in_array($fieldName, $entity->originalNames)) {
                $fileName = $file->name;
            } else {
                $time = new \DateTime('now');
                $fileName = $time->getTimestamp() . '.' . $file->extension;
            }
        } elseif (!$entity->isNewRecord && Yii::$app->controller->action->id != 'delete-image') {
            if ($entity->getAttribute($fieldName) != null) {
                $fileName = $entity->getAttribute($fieldName);
            } else {
                $this->deleteFile($entity, $fieldName);
            }
        }

        return $fileName;
    }

    /**
     * @param $entity
     * @param $fieldName
     * @param null $inputName
     * @return bool|null|UploadedFile
     * @throws \yii\base\InvalidConfigException
     */
    public function saveFile($entity, $fieldName, $inputName = null)
    {
        $file = $this->getFile($entity, $fieldName, $inputName);

        if (empty($file) || !$entity->getAttribute($fieldName)) {
            return false;
        }

        $filePath = $this->getDirPath($entity);
        $fullName = $this->getFullFileName($entity, $fieldName);

        $this->createDir($filePath);

        if (!$file->saveAs($fullName)) {
            $this->_errorCode = self::ERROR_UPLOAD_ERROR;
            return false;
        }

        return $file;
    }

    /**
     * @param $entity
     * @param $fieldName
     * @param null $inputName
     * @return null|UploadedFile
     * @throws \yii\base\InvalidConfigException
     */
    public function getFile($entity, $fieldName, $inputName = null)
    {
        $file = null;
        if (empty($inputName)) {
            $file = UploadedFile::getInstance($entity, $fieldName);
        } else {
            $file = UploadedFile::getInstanceByName($inputName);
        }

        if (isset($entity->size)){
            $entity->size = round($file->size / (1024*1024), 0);
        }

        if ($file == null) {
            $this->_errorCode = self::ERROR_NO_FILE;
        } elseif ($file->size == 0) {
            $this->_errorCode = self::ERROR_EMPTY_FILE;
            return null;
        } elseif (!in_array(FileHelper::getMimeType($file->tempName), array_merge(self::$imageMimeTypes, self::$videoMimeTypes))) {
            $this->_errorCode = self::ERROR_WRONG_FORMAT;
            return null;
        } elseif ($file->tempName == null) {
            $this->_errorCode = self::ERROR_NO_FILE;
            return null;
        }

        return $file;
    }

    /**
     * @param $entity
     */
    public function deleteFile($entity, $fieldName)
    {
        $filePath = $this->_storagePath . '/' . $this->getDirName($entity) . '/' . $entity->id;
        $fileName = $entity->getOldAttribute($fieldName);

        if (!empty($fileName)) {
            $filePath = $filePath . '/' . $fileName;
            if (file_exists($filePath)) {
                unlink($filePath);
            }
        }
    }

    /**
     * @param $entity
     */
    public function deleteAllFiles($entity)
    {
        $filePath = $this->getDirPath($entity);
        $this->clearDir($filePath);
        $this->deleteDir($filePath);
    }

    /**
     * @param $entity
     * @param $fieldName
     * @param $url
     * @return bool
     */
    public function copyFileFromUrl($entity, $fieldName, $url)
    {
        if (empty($url)) {
            return false;
        }

        $extension = pathinfo($url)['extension'];
        if (strpos($extension, '?') !== false) {
            $extension = substr($extension, 0, strpos($extension, '?'));
        }

        $time = new \DateTime('now');
        $fileName = $time->getTimestamp() . '.' . $extension;

        $entity->setAttribute($fieldName, $fileName);

        $filePath = $this->getDirPath($entity);
        $fullName = $this->getFullFileName($entity, $fieldName);

        $this->createDir($filePath);

        $fileContent = file_get_contents($url);
        if (!file_put_contents($fullName, $fileContent)) {
            return false;
        }

        return true;
    }

    /**
     * @param $entity
     * @param $fieldName
     * @param string $prefix
     * @return mixed|string
     */
    public function getFileUrl($entity, $fieldName, $prefix = '')
    {
        if ($entity->getAttribute($fieldName) == null) {
            return null;
        }

        return $this->_storageUrl . '/'
            . $this->getDirName($entity) . '/'
            . $entity->id . '/'
            . $prefix
            . $entity->getAttribute($fieldName);
    }

    /**
     * @param $entity
     * @param $fieldName
     * @param string $prefix
     * @return mixed|string
     */
    public function getFilePath($entity, $fieldName, $prefix = '')
    {
        if ($entity->getAttribute($fieldName) == null) {
            return null;
        }

        return $this->_storagePath . '/'
            . $this->getDirName($entity) . '/'
            . $entity->id . '/'
            . $prefix
            . $entity->getAttribute($fieldName);
    }

    /**
     * @param string $path
     * @return string
     */
    public function setPath($path)
    {
        return $this->_storagePath = $path;
    }

    /**
     * @param $dir
     */
    protected function createDir($dir)
    {
        if (!is_dir($dir)) {
            mkdir($dir, 0777, true);
        }
    }

    /**
     * @param $dir
     */
    protected function clearDir($dir)
    {
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != '.' && $object != '..') {
                    unlink($dir . '/' . $object);
                }
            }
        }
    }

    /**
     * @param $dir
     */
    protected function deleteDir($dir)
    {
        if (is_dir($dir)) {
            rmdir($dir);
        }
    }

    /**
     * @param $entity
     * @return mixed
     */
    protected function getDirName($entity)
    {
        $entityClass = StringHelper::basename(get_class($entity));
        return $this->directories[$entityClass];
    }

    /**
     * @param $entity
     * @return string
     */
    protected function getDirPath($entity)
    {
        return $this->_storagePath . '/' . $this->getDirName($entity) . '/' . $entity->id;
    }

    /**
     * @param $entity
     * @param $fieldName
     * @return string
     */
    protected function getFullFileName($entity, $fieldName)
    {
        return $this->getDirPath($entity) . '/' . $entity->getAttribute($fieldName);
    }
}
